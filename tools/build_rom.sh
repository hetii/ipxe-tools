#!/bin/bash

# Example how to run it:
# ./build_rom.sh C25F1P03.bin 10ec 8168 /media/hetii/F45D-F043/
set -e

BIOS=${1}
VEN=${2}
DEV=${3}
DST=${4}
ROM_NAME=${VEN}${DEV}--ath9k.rom
CWD=`dirname $0`

if [ "${BIOS}" == "-h" -o "${BIOS}" == "--help" -o  ! -f "${BIOS}" -o -z "${VEN}" -o -z "${DEV}" ];then
  echo "Usage: ./build_rom.sh bios_file.rom lan_vendor_id lan_device_id destination_folder"
  exit
fi

if [ ! -d "${CWD}/ipxe" ]; then
  echo Stage 0 - missing ipxe project - clone one
  git clone git://git.ipxe.org/ipxe.git ${CWD}/ipxe
fi

CFG=${CWD}/ipxe/src/config/local/general.h
echo "#undef NET_PROTO_STP" > ${CFG}
#echo "#define DOWNLOAD_PROTO_NFS" >> ${CFG}
echo "#undef DOWNLOAD_PROTO_HTTP" >> ${CFG}
echo "#undef HTTP_AUTH_BASIC" >> ${CFG}
echo "#undef HTTP_AUTH_DIGEST" >> ${CFG}
echo "#undef CRYPTO_80211_WEP" >> ${CFG}
#echo "#undef CRYPTO_80211_WPA" >> ${CFG}
#echo "#undef CRYPTO_80211_WPA2" >> ${CFG}
echo "#undef SANBOOT_CMD" >> ${CFG}
echo "#undef SYNC_CMD" >> ${CFG}
echo "#undef LOGIN_CMD" >> ${CFG}
echo "#undef IBMGMT_CMD" >> ${CFG}
echo "#undef FCMGMT_CMD" >> ${CFG}
#echo "#undef NVO_CMD" >> ${CFG}
#echo "#undef AUTOBOOT_CMD" >> ${CFG}
#echo "#undef VNIC_IPOIB" >> ${CFG}
echo "#undef SANBOOT_PROTO_ISCSI" >> ${CFG}
echo "#undef SANBOOT_PROTO_AOE" >> ${CFG}
echo "#undef SANBOOT_PROTO_IB_SRP" >> ${CFG}
echo "#undef SANBOOT_PROTO_FCP" >> ${CFG}
echo "#undef SANBOOT_PROTO_HTTP" >> ${CFG}

echo Stage 1 - create ipxe boot rom.
if [ -f "${CWD}/ipxe/src/menu.ipxe" ];then
  LANG=C make -C ${CWD}/ipxe/src/ bin/${ROM_NAME} EMBED=menu.ipxe DEBUG=wpa,ath9k,wpa_psk,wpa_tkip,wpa_ccmp,net80211,sec80211,rc80211
else
  LANG=C make -C ${CWD}/ipxe/src/ bin/${ROM_NAME}
fi

LANG=C ${CWD}/ipxe/src/util/padimg.pl --verbose --blksize 4096 ${CWD}/ipxe/src/bin/${ROM_NAME}
echo Stage 2 - replace an existing linked PCI Option ROM module with VendorID: ${VEN} and DeviceID: ${DEV}
${CWD}/MMTool.exe ${BIOS} /r /l ${CWD}/ipxe/src/bin/${ROM_NAME} ${VEN} ${DEV}

if [ -d "${DST}" ];then
  echo Stage 3 - copy final bios file: ${BIOS} to destination: ${DST}
  cp ${BIOS} ${DST}
  sync && sync
fi

