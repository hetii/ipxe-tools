# ipxe-tools #

Collection of tools that help play with iPXE project.

### What is this repository for? ###

* This collection of tools is stored here to better understand iPXE project and the way how/when/with one rome can/should be used.

### How to update LAN boot rom with aptio bios. ###

* Clone this project: git clone https://hetii@bitbucket.org/hetii/ipxe-tools.git
* Check by lspci -nn vendor and pid value of your lan card.
* Download bios for your main board and if its "aptio" use script from tools/build_rom.sh
* eg: ./tools/build_rom.sh ../C25F1P03.bin 10ec 8168 /media/hetii/F45D-F043/
* Burn the bios into some bootable media and upgrade mainboard.
* Play with iPXE like usual.

### How to run dnsmasq as a tftp/pxe/dhcp server. ###

* Use tools/dnsmasq.conf and remove all double ## from dhcp server section.
* Comment all stuff in proxy section.

### How to run dnsmasq as a tftp/pxe/dhcp proxy. ###

* Use tools/dnsmasq.conf and uncoment everything from proxy section.
* Comment all stuff in server section.

### More info ###

http://www.richud.com/wiki/Network_iPXE_dnsmasq_Examples_PXE_BOOT
http://www.win-raid.com/t18f16-Guide-Manual-AMI-UEFI-BIOS-Modding.html